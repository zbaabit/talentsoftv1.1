﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WH_TalentSoft.Startup))]
namespace WH_TalentSoft
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
