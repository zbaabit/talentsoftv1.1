﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WH_TalentSoft.Models;

namespace WH_TalentSoft.Controllers
{
    public class TRAINEEsController : Controller
    {
        private Entities db = new Entities();

        // GET: TRAINEEs
        public ActionResult Index()
        {
            return View(db.TRAINEE.ToList());
        }

        // GET: TRAINEEs/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TRAINEE tRAINEE = db.TRAINEE.Find(id);
            if (tRAINEE == null)
            {
                return HttpNotFound();
            }
            return View(tRAINEE);
        }

        // GET: TRAINEEs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TRAINEEs/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Key,Réalisées,SUBS_V_STATUS_EMP,EMPLID,ctime,mtime")] TRAINEE tRAINEE)
        {
            if (ModelState.IsValid)
            {
                db.TRAINEE.Add(tRAINEE);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tRAINEE);
        }

        // GET: TRAINEEs/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TRAINEE tRAINEE = db.TRAINEE.Find(id);
            if (tRAINEE == null)
            {
                return HttpNotFound();
            }
            return View(tRAINEE);
        }

        // POST: TRAINEEs/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit([Bind(Include = "Key,Réalisées,SUBS_V_STATUS_EMP,EMPLID,ctime,mtime")] TRAINEE tRAINEE)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tRAINEE).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tRAINEE);
        }

        [HttpPost]
        public ActionResult Update(IEnumerable<TRAINEE> tRAINEEs)
        {
            if (ModelState.IsValid)
            {
                tRAINEEs.ToList().ForEach(t => db.Entry(t).State = EntityState.Modified);
                //db.Entry(tRAINEEs).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { result = "success", operation = "edittrainees" } , JsonRequestBehavior.AllowGet);
            }
            return Json(tRAINEEs, JsonRequestBehavior.AllowGet);
        }

        // GET: TRAINEEs/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TRAINEE tRAINEE = db.TRAINEE.Find(id);
            if (tRAINEE == null)
            {
                return HttpNotFound();
            }
            return View(tRAINEE);
        }

        // POST: TRAINEEs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            TRAINEE tRAINEE = db.TRAINEE.Find(id);
            db.TRAINEE.Remove(tRAINEE);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
