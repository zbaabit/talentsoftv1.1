﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WH_TalentSoft.Models;
using PagedList;

namespace WH_TalentSoft.Controllers
{
    public class ViewSessionsController : Controller
    {
        private Entities db = new Entities();


        public ActionResult Chat()
        {
            return View();
        }


        // GET: ViewSessions
        public ActionResult Index()
        {
            ViewBag.TypeFormation = db.ViewSESSION1.Where(t => t.Type_de_formation != null).Select(t => t.Type_de_formation).Distinct().ToList();
            ViewBag.StatutSession = db.ViewSESSION1.Where(t => t.Statut_de_la_session != null).Select(t => t.Statut_de_la_session).Distinct().ToList();
            ViewBag.Project = db.ViewSESSION1.Where(t => t.Projet != null).Select(t => t.Projet).Distinct().ToList();
            return View();
        }

        public ActionResult _ViewSession(int? page, string search1 = "", string search2 = "", string search3 = "", string search4 = "", string search5 = "", string search6 = "", string search7= "")
        {     
            var Formation = db.ViewSESSION1.AsQueryable();
            DateTime dateDebut;
            DateTime.TryParse(search1, out dateDebut);
            if (search1 != "") Formation = Formation.Where(s => s.Date_début_de_la_session.Value.CompareTo(dateDebut) == 0);
            if (search2 != "") Formation = Formation.Where(s => s.Code_de_la_session.Equals(search2));
            if (search3 != "") Formation = Formation.Where(s => s.Formateur.Contains(search3));
            if (search4 != "") Formation = Formation.Where(s => s.Projet.Contains(search4));
            if (search5 != "") Formation = Formation.Where(s => s.Type_de_formation.Equals(search5));
            if (search6 != "") Formation = Formation.Where(s => s.Statut_de_la_session.Equals(search6));
            if (search7 != "") Formation = Formation.Where(s => s.Etat_de_la_session.Equals(search7));
            var all = Formation.OrderBy(p => p.Date_début_de_la_session);   
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var resultToReturn = all.ToPagedList(pageNumber, pageSize);
            return PartialView(resultToReturn);       
        }

        public ActionResult Viewtrainee()
        {           
            ViewBag.pays = db.ViewTRAINEEs.Where(t => t.Pays != null).Select(t => t.Pays).Distinct().ToList();
            ViewBag.ppcLoc = db.ViewTRAINEEs.Where(t => t.DESC_PPC_Location != null).Select(t => t.DESC_PPC_Location).Distinct().ToList();
            
            return View();
        }
        public ActionResult ppcLocation(string pays)
        {          
            ViewBag.ppcLoc =(from m in db.ViewTRAINEEs where m.Pays==pays select m.DESC_PPC_Location ).Distinct().ToList();
            return Json(ViewBag.ppcLoc,JsonRequestBehavior.AllowGet);
        }

        public ActionResult _Viewtrainee(int? page, string search1 = "", string search2 = "", string search3 = "", string search4 = "")

        {
            var Formation = db.ViewTRAINEEs.AsQueryable();
            DateTime dateDebut;
            DateTime.TryParse(search1, out dateDebut);
            
            DateTime datefin;
            DateTime.TryParse(search2, out datefin);
            if (search1 != "") Formation = Formation.Where(s => s.Date_début_de_la_session.Value.CompareTo(dateDebut) == 0);
            if (search2 != "") Formation = Formation.Where(s => s.Date_fin_de_la_session.Value.CompareTo(datefin) == 0);
            if (search3 != "") Formation = Formation.Where(s => s.Pays.Equals(search3));
            if (search4 != "") Formation = Formation.Where(s => s.DESC_PPC_Location.Equals(search4));        
            var all = Formation.OrderBy(p => p.Date_début_de_la_session);
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var resultToReturn = all.ToPagedList(pageNumber, pageSize);
            return PartialView(resultToReturn);
         
        }


        // GET: ViewSessions/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewSESSION1 viewSession = db.ViewSESSION1.Find(id);
            if (viewSession == null)
            {
                return HttpNotFound();
            }
            return View(viewSession);
        }

        // GET: ViewSessions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ViewSessions/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Key,ID,Action,Code_de_la_session,Type_de_formation,Projet,Formateur,Nombre_de_stagiaire,Statut_de_la_session,Date_début_de_la_session,Date_fin_de_la_session")] ViewSESSION1 viewSession)
        {
            if (ModelState.IsValid)
            {
                db.ViewSESSION1.Add(viewSession);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(viewSession);
        }

        // GET: ViewSessions/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewSESSION1 viewSession = db.ViewSESSION1.Find(id);
            if (viewSession == null)
            {
                return HttpNotFound();
            }
            return View(viewSession);
        }

        // POST: ViewSessions/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Key,ID,Action,Code_de_la_session,Type_de_formation,Projet,Formateur,Nombre_de_stagiaire,Statut_de_la_session,Date_début_de_la_session,Date_fin_de_la_session")] ViewSESSION1 viewSession)
        {
            if (ModelState.IsValid)
            {
                db.Entry(viewSession).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(viewSession);
        }

        // GET: ViewSessions/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewSESSION1 viewSession = db.ViewSESSION1.Find(id);
            if (viewSession == null)
            {
                return HttpNotFound();
            }
            return View(viewSession);
        }

        // POST: ViewSessions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ViewSESSION1 viewSession = db.ViewSESSION1.Find(id);
            db.ViewSESSION1.Remove(viewSession);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public FileContentResult DownloadCSV(string _search1 = "", string _search2 = "", string _search3 = "", string _search4 = "")
        {

            var EmployeeQry = db.ViewTRAINEEs.AsQueryable();
         
            DateTime dateDebut;
            DateTime.TryParse(_search1, out dateDebut);

            DateTime datefin;
            DateTime.TryParse(_search2, out datefin);

            string csv = string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18};{19};{20}\n",
                "Entity",
                "Projet",
                "Théme",
                "Action de Formation",
                "Nom du Formateur",
                "Prénom du Formateur",
                "Id de la Session",
                "Date de Début",
                "Date de Fin",
                "Status de la Session",
                "Type de Formation",
                "Stagiaire",
                "Matricule",
                "Date de la Sous-Session",
                "Status de Validation",
                "Nombre d'heures Réalisées",
                "Service",
                "Code Emploi",
                "Desc Du Code Emploi",
                "Company",
                "Desc Of Company");

            if (_search1 != "") EmployeeQry = EmployeeQry.Where(s => s.Date_début_de_la_session.Value.CompareTo(dateDebut) == 0);
            if (_search2 != "") EmployeeQry = EmployeeQry.Where(s => s.Date_fin_de_la_session.Value.CompareTo(datefin) == 0);
            if (_search3 != "") EmployeeQry = EmployeeQry.Where(s => s.Pays.Equals(_search3));
            if (_search4 != "") EmployeeQry = EmployeeQry.Where(s => s.DESC_PPC_Location.Equals(_search4));
            csv += string.Concat(from employee in EmployeeQry.OrderBy(p => p.Date_début_de_la_session).ToList()
                                 select string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18};{19};{20}\n",
                                 employee.Pays,
                                 employee.Projet,
                                 employee.Theme,
                                 employee.Nom_de_l_action,
                                 employee.Nom_formateur_de_la_session,
                                 employee.Prénom_formateur_de_la_session,
                                 employee.Code_de_la_session,
                                 employee.Date_début_de_la_session,
                                 employee.Date_fin_de_la_session,
                                 employee.Statut_de_la_session,
                                 employee.Type_de_formation,
                                 employee.Stagiaire,
                                 employee.Matricule,
                                 employee.Date,
                                 employee.Etat_de_la_session,
                                 employee.Réalisées,
                                 "",
                                 employee.Code_emploi,
                                 employee.DESC_du_code_emploi,
                                 employee.Company,
                                 employee.Desc_of_company
                                       ));
            return File(new System.Text.UTF32Encoding().GetBytes(csv), "text/csv", "Rapport.csv");         

        }

    }
}
